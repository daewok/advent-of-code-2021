(uiop:define-package #:daewok-aoc-2021/day-6
  (:use #:cl))

(in-package #:daewok-aoc-2021/day-6)

(defparameter *test-input* "3,4,3,1,2")

(defparameter *days-to-reproduce* 7)
(defparameter *days-to-grow* 2)

(defclass school ()
  ((day
    :initform 0
    :accessor day)
   (timers
    :reader timers)
   (days-to-reproduce
    :reader days-to-reproduce
    :initform *days-to-reproduce*
    :initarg :days-to-reproduce)
   (days-to-grow
    :reader days-to-grow
    :initform *days-to-grow*
    :initarg :days-to-grow)))

(defmethod initialize-instance :after ((school school)
                                       &key
                                         (days-to-reproduce *days-to-reproduce*)
                                         (days-to-grow *days-to-grow*)
                                         initial-population)
  (setf (slot-value school 'timers) (make-array (+ days-to-reproduce days-to-grow)
                                                :initial-element 0))
  (loop :for fish :in initial-population
        :do (incf (aref (timers school) fish))))

(defgeneric max-internal-timer (school)
  (:method ((school school))
    (length (timers school))))

(defgeneric school-count (school value)
  (:method ((school school) value)
    (aref (timers school) value)))

(defgeneric school-total (school)
  (:method ((school school))
    (reduce #'+ (timers school))))

(defun shift-vector (vector &optional new-value)
  (let ((head (aref vector 0)))
    (loop :for i :below (1- (length vector))
          :do (setf (aref vector i) (aref vector (1+ i))))
    (setf (aref vector (1- (length vector))) new-value)
    head))

(defgeneric simulate (school num-days)
  (:method ((school school) num-days)
    (dotimes (i num-days)
      (incf (day school))
      (let* ((timers (timers school))
             (num-at-day-zero (shift-vector timers 0)))
        (incf (aref timers (1- (days-to-reproduce school))) num-at-day-zero)
        (incf (aref timers (1- (+ (days-to-reproduce school) (days-to-grow school)))) num-at-day-zero)))))

(defun make-school-from-stream (stream)
  (let* ((line (read-line stream))
         (initial-population (mapcar #'parse-integer (uiop:split-string line :separator '(#\,)))))
    (make-instance 'school :initial-population initial-population)))

(clim:define-border-type :bottom-only (stream left right bottom)
  (clim:draw-line* stream left bottom right bottom))

(clim:define-border-type :right-only (stream right bottom top)
  (clim:draw-line* stream right top right bottom))

(clim:define-presentation-method clim:present (school (type school) stream view &key)
  (clim:surrounding-output-with-border (stream)
    (format stream "Day: ~D~%" (day school))
    (clim:formatting-table (stream)
      (clim:with-text-style (stream (clim:make-text-style nil :bold :larger))
        (clim:surrounding-output-with-border (stream :shape :bottom-only)
          (clim:formatting-row (stream)
            (clim:surrounding-output-with-border (stream :shape :right-only)
              (clim:formatting-cell (stream :align-x :center)
                (princ "Internal Timer" stream)))
            (clim:formatting-cell (stream :align-x :center)
              (princ "Count" stream)))))
      (dotimes (i (max-internal-timer school))
        (clim:formatting-row (stream)
          (clim:surrounding-output-with-border (stream :shape :right-only)
            (clim:formatting-cell (stream :align-x :right)
              (princ i stream)))
          (clim:formatting-cell (stream :align-x :right)
            (princ (school-count school i)))))
      (clim:with-text-style (stream (clim:make-text-style nil :bold :larger))
        (clim:formatting-row (stream)
          (clim:surrounding-output-with-border (stream :shape :right-only)
            (clim:formatting-cell (stream :align-x :right)
              (princ "Total" stream)))
          (clim:formatting-cell (stream :align-x :right)
            (princ (school-total school) stream)))))))

(clim:define-application-frame day-6-frame ()
  ((school
    :accessor school))
  (:panes
   (int :interactor
        :height 200
        :width 400)
   (app :application
        :display-function 'display-app-pane
        :height 300
        :width 400))
  (:layouts
   (default (clim:vertically () app int))))

(define-day-6-frame-command (com-clear :name t) ()
  (clim:with-application-frame (frame)
    (slot-makunbound frame 'school)))

(define-day-6-frame-command (com-load-data :name t)
    ((pathname 'pathname :default (asdf:system-relative-pathname "daewok-aoc-2021" "input/day-6.txt")))
  (clim:with-application-frame (frame)
    (with-open-file (stream pathname)
      (setf (school frame) (make-school-from-stream stream)))))

(define-day-6-frame-command (com-load-test-data :name t) ()
  (clim:with-application-frame (frame)
    (with-input-from-string (stream *test-input*)
      (setf (school frame) (make-school-from-stream stream)))))

(define-day-6-frame-command (com-step :name t) ((days 'integer :default 1))
  (clim:with-application-frame (frame)
    (simulate (school frame) days)))

(define-day-6-frame-command (com-simulate-to-day :name t) ((days 'integer))
  (clim:with-application-frame (frame)
    (simulate (school frame) (- days (day (school frame))))))

(defun display-app-pane (frame pane)
  (when (slot-boundp frame 'school)
    (clim:present (school frame) 'school :stream pane)))

(defun run ()
  (clim:find-application-frame 'day-6-frame))

(uiop:define-package #:daewok-aoc-2021/day-3
  (:use #:cl))

(in-package #:daewok-aoc-2021/day-3)

(defun read-input ()
  "Reading the input is a bit more complicated this time. It's probably
unnecessary, but I like to be OCD. We read each line as a string, figure out
the length, and then READ-FROM-STRING as a binary. Ideally we could just READ
directly and compute the length using INTEGER-LENGTH, but it's possible that
the most significant bit in each entry is a zero, in which case we'd get the
incorrect answer."
  (let ((lines (uiop:read-file-lines (asdf:system-relative-pathname "daewok-aoc-2021" "input/day-3.txt"))))
    (values
     (mapcar #'(lambda (x) (uiop:with-safe-io-syntax ()
                             (let ((*read-base* 2)) (read-from-string x))))
             lines)
     (reduce #'max lines :key #'length))))

(defun day-3-a ()
  (multiple-value-bind (data num-bits) (read-input)
    (let ((gamma 0)
          (data-length (length data))
          epsilon)
      (dotimes (index num-bits)
        (loop :for num :in data
              :count (plusp (ldb (byte 1 index) num)) :into count
              :finally
                 (when (> count (/ data-length 2))
                   (setf (ldb (byte 1 index) gamma) 1))))
      (setf epsilon (logxor gamma (1- (expt 2 num-bits))))
      (values (* epsilon gamma)))))

(defun bit-counts (data bit-index)
  "Returns two VALUES, the number of zeros in DATA at BIT-INDEX, and the number
of ones."
  (loop :for num :in data
        :for value := (ldb (byte 1 bit-index) num)
        :count (zerop value) :into zeros
        :count (plusp value) :into ones
        :finally (return (values zeros ones))))

(defun filter-by-bit (data bit-index most-or-least)
  "Removes all numbers from DATA that do not have the bit at BIT-INDEX set to
the :MOST (or :LEAST) common value."
  (flet ((bit-accessor-function (x)
           (ldb (byte 1 bit-index) x)))
    (multiple-value-bind (zeros ones) (bit-counts data bit-index)
      (ecase most-or-least
        (:most
         (remove (if (>= ones zeros) 0 1) data :test #'= :key #'bit-accessor-function))
        (:least
         (remove (if (<= zeros ones) 1 0) data :test #'= :key #'bit-accessor-function))))))

(defun filter-by (data num-bits most-or-least)
  "Removes all numbers from DATA by repeated applications of FILTER-BY-BIT
until one number remains."
  (loop :for bit-index :from (1- num-bits) :downto 0
        :for current-data := data :then filtered-data
        :for filtered-data := (filter-by-bit current-data bit-index most-or-least)
        :until (= (length filtered-data) 1)
        :finally (return (first filtered-data))))

(defun day-3-b ()
  (multiple-value-bind (data num-bits) (read-input)
    (let ((o2-rating (filter-by data num-bits :most))
          (co2-rating (filter-by data num-bits :least)))
      (values (* o2-rating co2-rating)
              o2-rating
              co2-rating))))

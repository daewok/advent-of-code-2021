(uiop:define-package #:daewok-aoc-2021/day-2
  (:use #:cl))

(in-package #:daewok-aoc-2021/day-2)

(defun read-input ()
  (uiop:with-safe-io-syntax (:package :keyword)
    (uiop:read-file-forms (asdf:system-relative-pathname "daewok-aoc-2021" "input/day-2.txt"))))

(defun day-2-a ()
  (let ((data (read-input)))
    (loop :for command :in data :by #'cddr
          :for arg :in (rest data) :by #'cddr
          :if (eql command :forward)
            :sum arg :into position
          :else
            :sum (if (eql command :down) arg (- arg)) :into depth
          :finally
             (return (* depth position)))))

(defun day-2-b ()
  (let ((data (read-input)))
    (loop :for command :in data :by #'cddr
          :for arg :in (rest data) :by #'cddr
          :if (eql command :forward)
            :sum arg :into position
            :and :sum (* arg aim) :into depth
          :else
            :sum (if (eql command :down) arg (- arg)) :into aim
          :finally
             (return (* depth position)))))

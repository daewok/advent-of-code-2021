(uiop:define-package #:daewok-aoc-2021/day-4
  (:use #:cl)
  (:local-nicknames (#:ss #:split-sequence)))

(in-package #:daewok-aoc-2021/day-4)

(defun read-delimited-line (stream &key (eof-error-p t) eof-value
                                     (delimiter #\,) (transform 'read-from-string))
  (let ((line (read-line stream eof-error-p stream)))
    (if (eq line stream)
        eof-value
        (mapcar transform (ss:split-sequence delimiter line :remove-empty-subseqs t)))))

(defun read-2d-array (stream)
  (loop
    :for line := (read-delimited-line stream :eof-error-p nil :delimiter #\Space)
    :until (null line)
    :collect line :into lines
    :finally (return (make-array (list (length lines) (length (first lines)))
                                 :initial-contents lines))))

(defclass bingo-card ()
  ((numbers
    :initarg :numbers
    :reader numbers)
   (marked
    :reader marked)))

(defmethod initialize-instance :after ((card bingo-card) &key numbers)
  (setf (slot-value card 'marked)
        (make-array (array-dimensions numbers) :element-type 'boolean :initial-element nil)))

(defun read-input ()
  (uiop:with-safe-io-syntax ()
    (with-open-file (stream (asdf:system-relative-pathname "daewok-aoc-2021" "input/day-4.txt"))
      (values
       (read-delimited-line stream)
       (loop
         :until (eql stream (peek-char nil stream nil stream))
         :collect (make-instance 'bingo-card :numbers (read-2d-array stream)))))))

(defgeneric mark-number (card number)
  (:method ((card bingo-card) number)
    (destructuring-bind (rows columns) (array-dimensions (numbers card))
      (dotimes (row rows)
        (dotimes (col columns)
          (when (= number (aref (numbers card) row col))
            (setf (aref (marked card) row col) t)))))))

(defgeneric card-wins-p (card)
  (:method ((card bingo-card))
    (destructuring-bind (rows columns) (array-dimensions (numbers card))
      (or
       ;; Check rows.
       (loop
         :for row :below rows
           :thereis (loop :for col :below columns
                          :always (aref (marked card) row col)))
       ;; Check columns.
       (loop
         :for col :below columns
           :thereis (loop :for row :below rows
                          :always (aref (marked card) row col)))))))

(defgeneric unmarked-sum (card)
  (:method ((card bingo-card))
    (destructuring-bind (rows columns) (array-dimensions (numbers card))
      (loop :for row :below rows
            :sum (loop :for col :below columns
                       :unless (aref (marked card) row col)
                         :sum (aref (numbers card) row col))))))

(defun day-4-a ()
  (multiple-value-bind (picks cards) (read-input)
    (let (winning-card
          last-pick)
      (dolist (pick picks)
        (setf winning-card
              (loop :for card :in cards
                    :do (mark-number card pick)
                      :thereis (when (card-wins-p card) card))
              last-pick pick)
        (unless (null winning-card)
          (return)))
      (* last-pick (unmarked-sum winning-card)))))

(defun day-4-b ()
  (multiple-value-bind (picks cards) (read-input)
    (let (winning-cards)
      (dolist (pick picks)
        (loop :for card :in cards
              :do (mark-number card pick)
              :when (card-wins-p card)
                :do (push (cons card pick) winning-cards)
                    (setf cards (remove card cards)))
        (when (null cards)
          (return)))
      (destructuring-bind (last-winner . last-pick) (first winning-cards)
        (* last-pick (unmarked-sum last-winner))))))

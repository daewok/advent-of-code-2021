(uiop:define-package #:daewok-aoc-2021/day-5
  (:use #:cl)
  (:local-nicknames
   (#:alex #:alexandria)
   (#:regex #:cl-ppcre)))

(in-package #:daewok-aoc-2021/day-5)

(defclass point ()
  ((x
    :initarg :x
    :reader x)
   (y
    :initarg :y
    :reader y)))

(defclass line ()
  ((from-point
    :initarg :from-point
    :reader from-point)
   (to-point
    :initarg :to-point
    :reader to-point)))

(defgeneric line-horizontal-p (line)
  (:method ((line line))
    (= (y (from-point line)) (y (to-point line)))))

(defgeneric line-vertical-p (line)
  (:method ((line line))
    (= (x (from-point line)) (x (to-point line)))))

(defgeneric line-diagonal-p (line)
  (:method ((line line))
    (and (not (line-vertical-p line))
         (not (line-horizontal-p line)))))

(defclass vent-map ()
  ((lines
    :initform nil
    :accessor lines)
   (occlusion-grid
    :accessor occlusion-grid)))

(defmethod initialize-instance :after ((map vent-map) &key size-x size-y)
  (setf (occlusion-grid map) (make-array (list size-x size-y)
                                         :initial-element 0
                                         :element-type 'fixnum)))

(defgeneric add-line (map line)
  (:method ((map vent-map) (line line))
    (push line (lines map))
    (let* ((from-x (x (from-point line)))
           (from-y (y (from-point line)))
           (to-x (x (to-point line)))
           (to-y (y (to-point line))))
      (loop :with x := from-x
            :with y := from-y
            :repeat (1+ (max (abs (- from-x to-x))
                             (abs (- from-y to-y))))
            :do
               (incf (aref (occlusion-grid map) x y))
               (cond
                 ((< from-x to-x)
                  (incf x))
                 ((> from-x to-x)
                  (decf x)))
               (cond
                 ((< from-y to-y)
                  (incf y))
                 ((> from-y to-y)
                  (decf y)))))))

(defgeneric count-intersections (map)
  (:method ((map vent-map))
    (destructuring-bind (size-x size-y) (array-dimensions (occlusion-grid map))
      (loop :for x :below size-x
            :sum (loop :for y :below size-y
                       :count (>= (aref (occlusion-grid map) x y) 2))))))

(clim:define-application-frame aoc-frame ()
  ((vent-map
    :accessor vent-map)
   (unadded-lines
    :initform nil
    :accessor unadded-lines))
  (:panes
   (int :interactor
        :height 200
        :width 600)
   (detail :application
           :display-time nil)
   (vent-map :application
             :display-function 'display-vent-map
             :height 400
             :width 600))
  (:layouts
   (default (clim:horizontally ()
              (clim:vertically () detail int)
              vent-map))))

(defun read-line-from-stream (stream)
  (let ((line (read-line stream)))
    (regex:register-groups-bind ((#'parse-integer from-x from-y to-x to-y))
        ("(\\d+),(\\d+) -> (\\d+),(\\d+)" line)
      (make-instance 'line
                      :from-point (make-instance 'point :x from-x :y from-y)
                      :to-point (make-instance 'point :x to-x :y to-y)))))

(define-aoc-frame-command (com-clear :name t) ()
  (clim:with-application-frame (frame)
    (slot-makunbound frame 'vent-map)
    (setf (unadded-lines frame) nil)))

(define-aoc-frame-command (com-load-data :name t)
    ((pathname 'pathname :default (asdf:system-relative-pathname "daewok-aoc-2021" "input/day-5.txt")))
  (clim:with-application-frame (frame)
    (with-accessors ((unadded-lines unadded-lines)
                     (vent-map vent-map))
        frame
      (let ((max-x most-negative-fixnum)
            (max-y most-negative-fixnum))
        (uiop:with-safe-io-syntax ()
          (with-open-file (stream pathname)
            (handler-case
                (loop
                  (let ((line (read-line-from-stream stream)))
                    (setf max-x (max max-x (x (from-point line)) (x (to-point line)))
                          max-y (max max-y (y (from-point line)) (y (to-point line))))
                    (push line unadded-lines)))
              (end-of-file () nil))))
        (setf unadded-lines (nreverse unadded-lines))
        (setf vent-map (make-instance 'vent-map :size-x (1+ max-x) :size-y (1+ max-y)))
        (format t "Read ~D lines~%" (length unadded-lines))
        (format t "Created map. Size: (~D, ~D)~%" (1+ max-x) (1+ max-y))))))

(define-aoc-frame-command (com-add-lines :name t) ((direction '(member :horizontal :vertical :diagonal)))
  (clim:with-application-frame (frame)
    (with-accessors ((unadded-lines unadded-lines)
                     (vent-map vent-map))
        frame
      (let* ((test (ecase direction
                     (:horizontal 'line-horizontal-p)
                     (:vertical 'line-vertical-p)
                     (:diagonal 'line-diagonal-p)))
             (lines (remove-if-not test unadded-lines)))
        (setf unadded-lines (remove-if test unadded-lines))
        (loop :for line :in lines
              :do (add-line vent-map line))))))

(define-aoc-frame-command (com-count-intersections :name t) ()
  (clim:with-application-frame (frame)
    (format t "There are ~D intersections~%" (count-intersections (vent-map frame)))))

(defun display-vent-map (frame pane)
  (when (slot-boundp frame 'vent-map)
    (with-accessors ((vent-map vent-map)) frame
      (multiple-value-bind (pane-width pane-height)
          (clim:bounding-rectangle-size pane)
        (destructuring-bind (map-width map-height) (array-dimensions (occlusion-grid vent-map))
          (clim:with-scaling (pane (min (/ pane-width map-width) (/ pane-height map-height)))
            (loop
              :for line :in (lines vent-map)
              :do (clim:draw-line* pane
                                   (x (from-point line)) (y (from-point line))
                                   (x (to-point line)) (y (to-point line))))))))))

(defun run ()
  (clim:find-application-frame 'aoc-frame))

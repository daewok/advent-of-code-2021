(uiop:define-package #:daewok-aoc-2021/day-1
  (:use #:cl))

(in-package #:daewok-aoc-2021/day-1)

(defun read-input ()
  (uiop:with-safe-io-syntax ()
    (uiop:read-file-forms (asdf:system-relative-pathname "daewok-aoc-2021" "input/day-1.txt"))))

(defun day-1-a ()
  (let ((data (read-input)))
    (loop :for x :in data
          :for y :in (rest data)
          :count (> y x))))

(defun day-1-b ()
  (let ((data (read-input)))
    (loop :for (x-1 x-2 x-3) :on data
          :for (y-1 y-2 y-3) :on (rest data)
          :until (null y-3)
          :count (> (+ y-1 y-2 y-3) (+ x-1 x-2 x-3)))))

(defun sum-window-or-nil (elements length)
  "Returns NIL if there are fewer ELEMENTS than LENGTH."
  (loop :repeat length
        :for element := (pop elements)
        :when (null element)
          :return nil
        :sum element))

(defun day-1-b-2 (&key (window-length 3))
  (let ((data (read-input)))
    (loop :for x :on data
          :for y :on (rest data)
          :for x-sum := (sum-window-or-nil x window-length)
          :for y-sum := (sum-window-or-nil y window-length)
          :until (null y-sum)
          :count (> y-sum x-sum))))

(defun day-1-b-3 (&key (window-length 3))
  (let ((data (read-input)))
    (loop :for x :in data
          :for y :in (nthcdr window-length data)
          :count (> y x))))


(defsystem #:daewok-aoc-2021
  :name "AOC 2021"
  :description "Advent of Code 2021"
  :author "Eric Timmons <etimmons@mit.edu>"
  :maintainer "Eric Timmons <etimmons@mit.edu>"
  :pathname "src"
  :depends-on ("alexandria" "split-sequence" "clim" "cl-ppcre" "mcclim")
  :components ((:file "day-1")
               (:file "day-2")
               (:file "day-3")
               (:file "day-4")
               (:file "day-5")
               (:file "day-6")))
